<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dislikesoncomment extends Model
{
    public function comment(){

        return $this->belongsTo(Comment::class);
    }
}
