<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    //
    protected $fillable = [
        'id',
        'post_title',
        'post_body',
        'category_id',
        'post_image',
        'profile_id',
        'comment_id',
       
    ];
    public function profiles(){
        return $this->belongsTo('App\Profile');

    }
    public function userss(){
        return $this->belongsTo(User::class);

    }
    public function likes(){
        return $this->belongsTo('App\Like', 'like_id');

    }
    public function users(){
        return $this->hasMany(Comment::class);

    }
    public function use(){
        return $this->hasMany(Comment::class);

    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public function categories(){
        return $this->belongsTo('App\Category');

    }

    
}








