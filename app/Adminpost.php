<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adminpost extends Model
{
    //
    protected $fillable = [
        'id',
        'post_title',
        'post_body',
        'category_id',
        'post_image',
        'profile_id',
        'comment_id',
        'user_id'
    ];
    public function profiles(){
        return $this->belongsTo('App\Profile', 'profile_id');

    }
    public function users(){
        return $this->belongsTo('App\User', 'user_id');

    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }


    
}
