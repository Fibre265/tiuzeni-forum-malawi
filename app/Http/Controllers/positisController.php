<?php

namespace App\Http\Controllers;

use App\Post;
use App\Adminpost;
use Illuminate\Http\Request;
use App\Category;
use App\Profile;
use App\About;
use App\Reply;
use App\Dislikesoncomment;
use App\Notification;


use App\User;
use App\Comment;
use App\Like;
use App\Likesoncomment;
use App\Dislike;
use DB;


use Illuminate\Support\Facades\Auth;

class positisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $about = About::all();      
        $postAd = Adminpost::all(); 
        $users = User::find(['users.id']); 

        $postH = Post::paginate(4, ['*'], '1pagination');
        $posta = Post::where('user_id','<', '3')
        ->paginate(3, ['*'], '2pagination');
        $postaa = Post::where('user_id','>', '2')
        ->paginate(3, ['*'], '2pagination');


        $cat_idcount= DB::table('posts')
        ->join('categories', 'posts.category_id','=','categories.id')
        ->select('posts.*' )
        ->where(['posts.id' => '2'])
        ->get();

      
        $use = User::where('id');
        $AdminPost = Adminpost::all();  
        $poID = Category::find('id');
        $getconc = Post::where( 'id', '=', $poID)->get();
        $cat_id = Category::with('posts')->simplePaginate(5);
        $noti = Notification::paginate(2);
            return view('dead', ['postH'=>$postH,
                                 'about'=>$about,
                                 'cat_idcount'=>$cat_idcount,
                                 'postaa'=>$postaa,
                                 'use'=>$use,
                                 'postAd'=>$postAd,
                                 'users'=>$users,
                                 'posta'=>$posta,
                                'cat_id'=>$cat_id, 
                                'getconc'=>$getconc,
                                'AdminPost'=>$AdminPost,
                                'noti'=>$noti]);                          
    }

    public function users(){   

        
        if(Auth::guest()){

            $users = User::all();
            $noti = Notification::all();
            $about = About::all();
            $cat_id = Category::with('posts')->paginate(10);
            $postH = Post::paginate(4, ['*'], '1pagination');
            return view('auth.login',  ['about'=>$about,
                                   'postH'=>$postH,
                                  'cat_id'=>$cat_id,
                                  'noti'=>$noti,
                                  'users'=>$users,
                                 ]);
       
  
        }
        if(auth::id()<3){
        $users = User::all();
        $noti = Notification::all();
        $about = About::all();
        $cat_id = Category::with('posts')->paginate(10);
        $postH = Post::paginate(4, ['*'], '1pagination');
        return view('users',  ['about'=>$about,
                               'postH'=>$postH,
                              'cat_id'=>$cat_id,
                              'noti'=>$noti,
                              'users'=>$users,
                             ]);
        }
        return redirect()->back();
        
    }



    public function deleteUser($id){
        User::where('id', $id)->delete();
        return redirect()->back();
    }

    public function selfDeleteUser(){
        $user_id = Auth::user()->id;
        User::where('id', $user_id)->delete();
        return redirect()->back();
    }



    public function search(Request $request){
                  
        $keyword = $request->input('search');
        $posts = Post::where('post_title', 'LIKE', '%' .$keyword.'%')->get();
        $about = About::all();
        $comment = DB:: table( 'posts')
        ->crossJoin('users')->get();
        $postH = Post::paginate(4, ['*'], '1pagination');
        $postAd = Adminpost::all(); 
        $users = User::find(['users.id']);
        $posta = Post::paginate(5);
        $use = User::where('id');
        $cat_id = Category::with('posts')->paginate(8);
        $noti = Notification::all();

        return view('searchposts', ['noti'=>$noti,
                                    'about'=>$about,
                                    'postH'=>$postH,
                                    'posts'=>$posts,
                                    'use'=>$use,
                                    'comment'=>$comment,
                                    'postAd'=>$postAd,
                                    'users'=>$users,
                                    'posta'=>$posta,
                                    'cat_id'=>$cat_id
                                    ]);
    }

    public function firstpage(){
        $noti = Notification::all();
        $user_each = User::all();
        $about = About::all();
        $post = Adminpost::all();   
        $comment = Comment::all();   
        $cat_id = Category::paginate(10);
        return view('firstpage', ['about'=>$about,
                                  'post'=>$post,
                                  'comment'=>$comment,
                                  'cat_id'=>$cat_id,
                                   'noti'=>$noti,
                                   ]);

    }

    public function commentsList($post_id){
        $about = About::all();
        $noti = Notification::all();
        $postH = Post::paginate(4, ['*'], '1pagination');
        $commentnone = DB::table('posts') 
        ->join('comments', 'posts.id', '=', 'comments.post_id')
         ->select( 'comments.*') 
         ->where(['posts.id' => $post_id, 'comments.user_id'=> NULL])
         ->paginate(6);     
        $likepost = Post::find($post_id);
        $likecou = Like::where(['post_id' => $likepost ->id])->count();
        $dislikecou = Dislike::where(['post_id' => $likepost ->id])->count();

        $likecomment = Post::find($post_id);
        $likecommentcou = Like::where(['post_id' => $likepost ->id])->count();
        $dislikecommentcou = Dislike::where(['post_id' => $likepost ->id])->count();
        $likesoncommet= Comment::where('post_id', '=', $post_id);
        $dislikesoncommet= Comment::where('post_id', '=', $post_id);

        $cat_idcount= DB::table('posts')
        ->join('categories', 'posts.category_id','=','categories.id')
        ->select('posts.*' )
        ->where(['posts.id' => $post_id])
        ->get();


        $post = post::where( 'id', '=', $post_id)->get();
        $cat_id= DB::table('posts')
        ->join('categories', 'posts.category_id','=','categories.id')
        ->select('categories.category','categories.id','posts.id' )
        ->where(['posts.id' => $post_id])
        ->paginate(2);
        $comment = Post::where('id', '=', $post_id)->get();
        $comme = Comment::where('post_id', '=', $post_id)->paginate(8);

        return view('posts.CommentsView',[
                                    'cat_idcount' => $cat_idcount,
                                    'about' => $about,
                                    'noti' => $noti,
                                    'postH' => $postH,
                                    'commentnone'  => $commentnone,
                                    'likecou' => $likecou,
                                    'dislikecou' => $dislikecou, 
                                    'likecommentcou' => $likecommentcou,
                                    'dislikecommentcou' => $dislikecommentcou,
                                    'post_id'=>$post_id,
                                    'comment'=>$comment,
                                    'post'=>$post,'cat_id'=>$cat_id,
                                    'comme'=>$comme,
                                    'likesoncommet'=>$likesoncommet,
                                    'dislikesoncommet'=>$dislikesoncommet]);

    }
    public function allReplies($id){ 
        $about = About::all();
        $noti = Notification::all();
        $postH = Post::paginate(4, ['*'], '1pagination');   
        $comment = Comment::where('id', '=', $id)->get();
        $Reply = Reply::where('comment_id', '=', $id)->simplePaginate(5);
        $cat_id= Category::paginate(3);
        $category= Category::all();
        $post = Post::where('id', '=', $id)->get();  

     return view('posts.repliesall',[
        
                'about' => $about,
                'noti' => $noti,
                'postH' => $postH,
                'cat_id'=>$cat_id,
                'comment'=>$comment,
                'Reply'=>$Reply,
                'category'=>$category,
                'post'=>$post,
            
     ]);

    }

    public function deleteComment(Request $request, $post_id){

        if(Auth::guest()){
            $noti = Notification::all();
            $about = About::all();
            return view('auth.register',['noti'=>$noti,'about'=>$about]);
        }
            Comment::where('id', $post_id)
            ->delete();
            return redirect()->back()->with('response','comment deleted');
    }
    public function deleteReply(Request $request, $post_id){

        if(Auth::guest()){
            $likepost = Post::find($post_id);
            $likecou = Like::where(['post_id' => $likepost ->id])->count();
            $dislikecou = Dislike::where(['post_id' => $likepost ->id])->count();
            $post = post::where( 'id', '=', $post_id)->get();
    
        $comment = DB::table('users') 
        ->join('comments', 'users.id', '=', 'comments.user_id')
        ->join('posts', 'comments.post_id', '=', 'posts.id')
         ->select('users.name', 'comments.*') 
         ->where(['posts.id' => $post_id])
         ->get();
        $cat_id = Category::paginate(11);
            return view('dead',['likecou' => $likecou, 
                                'dislikecou' => $dislikecou,
                                'post_id'=>$post_id,
                                'comment'=>$comment,
                                'post'=>$post,
                                'cat_id'=>$cat_id]);
        }


        Reply::where('id', $post_id)
        ->delete();

        return redirect()->back()->with('response','comment deleted');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function likecomment($id){

        $loggedin_user = Auth::user()->id;
        $like_user =Likesoncomment::where(['user_id' => $loggedin_user, 'id' => $id])->first();
        if(empty($like_user->user_id)){

            $user_id = Auth::user()->id;

            $id = $id;
            $like =new Likesoncomment;
            $like->user_id = $user_id;
        
            $like->comment_id = $id;
            $like->save();
            return redirect()->back();

        }  
        else{
            return redirect()->back();
        }
    
    }

     public function dislikecomment($id){

        $loggedin_user = Auth::user()->id;
        $like_user =Dislikesoncomment::where(['user_id' => $loggedin_user, 'id' => $id])->first();
        if(empty($like_user->user_id)){

            $user_id = Auth::user()->id;  
            $id = $id;
            $like =new Dislikesoncomment;
            $like->user_id = $user_id;
        
            $like->comment_id = $id;
            $like->save();
            return redirect()->back();

        }  
        else{
            return redirect()->back();
        }     
    }

     public function like($post_id){

        $loggedin_user = Auth::user()->id;
        $like_user =like::where(['user_id' => $loggedin_user, 'post_id' => $post_id])->first();
        if(empty($like_user->user_id)){

            $user_id = Auth::user()->id;
           
            $post_id = $post_id;
            $like =new Like;
            $like->user_id = $user_id;
        
            $like->post_id = $post_id;
            $like->save();
            return redirect()->back();

        }  
        else{
            return redirect()->back();
        }


        
    }
     public function dislike($post_id){

        $loggedin_user = Auth::user()->id;
        $like_user =Dislike::where(['user_id' => $loggedin_user, 'post_id' => $post_id])->first();
        if(empty($like_user->user_id)){
            $user_id = Auth::user()->id;  
            $post_id = $post_id;
            $like =new Dislike;
            $like->user_id = $user_id;   
            $like->post_id = $post_id;
            $like->save();
            return redirect()->back();

        }  
        else{
            return redirect()->back();
        }

    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
