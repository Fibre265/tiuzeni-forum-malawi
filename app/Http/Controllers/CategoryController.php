<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Profile;
use App\Notification;
use App\User;
use App\Post;
use App\About;
use App\Adminpost;
use App\Comment;
use DB;
use Auth;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function availableCategories()
    {
          
            $cat_id = Category::paginate(11);
            $category = Category::all();
            $users = User::all();
            $postH = Post::paginate(4, ['*'], '1pagination');
            $noti = Notification::all();
            $about = About::all();
                     
            return view('categories.availableCategories', 
             ['about' => $about,
             'noti' => $noti,
             'category' => $category,
             'postH' =>$postH,
             'cat_id' => $cat_id,
             'users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $category = Category::all();
        $postH = Post::paginate(4, ['*'], '1pagination'); 
        $about = About::all();
        $proComment = Post::with('Comments')->get();
        $cat_id = Category::paginate(11); 
        $noti = Notification::all();

        return view('categories.category',[
            'about' => $about,
            'category' => $category,
            'postH' => $postH,
            'proComment'=>$proComment,
            'cat_id'=>$cat_id,
            'noti'=>$noti]);
      
 
    }
    public function addAboutForm()
    {
        if(Auth::guest()){

            $users = User::all();
            $noti = Notification::all();
            $about = About::all();
            $cat_id = Category::with('posts')->paginate(10);
            $postH = Post::paginate(4, ['*'], '1pagination');
            return view('auth.login',  ['about'=>$about,
                                   'postH'=>$postH,
                                  'cat_id'=>$cat_id,
                                  'noti'=>$noti,
                                  'users'=>$users,
                                 ]);
       
  
        }
        {
            
        if(auth::id() <3){
        $category = Category::all();
        $postH = Post::paginate(4, ['*'], '1pagination');
        $proComment = Post::with('Comments')->get();
        $cat_id = Category::paginate(11);
        $noti = Notification::all();
        $about = About::all();

        return view('categories.aboutForm',[
        'about'=>$about,
        'category' => $category,
        'postH' => $postH,
        'cat_id'=>$cat_id,
        'noti'=>$noti]);
    }
    return redirect()->back();
        }
    }

    public function aboutView()
    {

        $category = Category::all();
        $postH = Post::paginate(4, ['*'], '1pagination');
        $cat_id = Category::paginate(11);
        $noti = Notification::all();
        $about = About::all();

        return view('categories.aboutView',[
        'about' => $about,
        'postH' => $postH,
        'cat_id'=>$cat_id,
        'category'=>$category,
        'noti'=>$noti]); 
    }

    public function aboutEdit($id)
    {

        $category = Category::all();
        $postH = Post::paginate(4, ['*'], '1pagination');
        $proComment = Post::with('Comments')->get();
        $cat_id = Category::paginate(11);
        $noti = Notification::all();
        $aboutv = About::findOrFail($id);
        $about = About::all();
        
        return view('categories.aboutEditForm',[
        'aboutv' => $aboutv,
        'about' => $about,
        'category' => $category,
        'postH' => $postH,
        'proComment'=>$proComment,
        'cat_id'=>$cat_id,'noti'=>$noti]);
      
 
    }
    public function aboutCreating(Request $request)
            {
            $about = About::create([
                    'about_tiuzeni_forum' => $request->input('about_tiuzeni_forum'),
                    'user_task' =>$request->input('user_task'),
                    'user_prohibition' =>$request->input('user_prohibition')
            ]);
            return redirect()->back();
            }



    public function aboutEditUpdate(Request $request, $id)
    {
            $aboutUpdated = About::where('id', $id)->update([
            'about_tiuzeni_forum' => $request->input('about_tiuzeni_forum'),
            'user_task' =>$request->input('user_task'),
            'user_prohibition' =>$request->input('user_prohibition')
            ]);



            $category = Category::all();
            $postH = Post::paginate(4, ['*'], '1pagination');
            $user_id = Auth::user()->id;
            $testprofile = User::where( 'id', '=', $user_id)->get();
            $proComment = Post::with('Comments')->get();
            $cat_id = Category::paginate(11);
            $noti = Notification::all();
            $about = About::all();


            return view('categories.aboutView',[
                                    'about' => $about,
                                    'aboutUpdated' => $aboutUpdated,
                                    'category' => $category,
                                    'postH' => $postH,
                                    'proComment'=>$proComment,
                                    'cat_id'=>$cat_id,'noti'=>$noti]);
}


    public function addingNotification(Request $request)
    {

        $not = Notification::create([
            'notice' => $request->input('notice')
                                 ]);
        

        $noti = Notification::all();
        $comment = DB:: table( 'posts' )
        ->crossJoin('users')
        ->get();
        $about = About::all();
        $postAd = Adminpost::all(); 
        $users = User::find(['users.id']);
        $postizo = Post::with('comments','users')->get();
        $postH = Post::paginate(4, ['*'], '1pagination');

        $posta = Post::where('user_id','>', '1')
        ->paginate(3, ['*'], '2pagination');

        $postaa = Post::where('user_id','=', '1')
        ->paginate(3, ['*'], '2pagination');

        $postaa->setPageName('other_page');
        $use = User::where('id');

        $AdminPost = Adminpost::all();  
        $poID = Category::find('id');
        $getconc = Post::where( 'id', '=', $poID)->get();
        $cat_id = Category::with('posts')->paginate(10);

        return view('dead',['about'=>$about,
                            'not'=>$not,
                            'noti'=>$noti,
                            'postH'=>$postH,
                            'postaa'=>$postaa,
                            'use'=>$use,
                            'comment'=>$comment,
                            'postAd'=>$postAd,
                            'users'=>$users,
                            'posta'=>$posta,
                            'postizo'=>$postizo,
                            'cat_id'=>$cat_id,
                            'getconc'=>$getconc,
                            'AdminPost'=>$AdminPost]);    
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cat_id = Category::paginate(11);
        $postH = Post::paginate(4, ['*'], '1pagination');      
        $noti= Notification::all();
        $category = Category::create([
            'category' => $request->input('category'),
       
                
                 ]);
                //  categories.availableCategories
                return redirect('/firstpage')->  
                with('responsee', 'the Topic has been Added Successfully');
        

             
             
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cat = Category::find($category->id);

        return view('categories.category', ['cat'=>$cat]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Category::where('id','=',$id)->delete();
        return redirect()->back()->with('success', 'category deleted');
    }

    public function deleteNotice($id)
    {
        Notification::find($id)->delete();
     
        return redirect()->back();
    }
    public function seeAllNotification()
    {
        $noti = Notification::all();
        $about = About::all();
        $cat_id = Category::paginate(11);
        $postH = Post::paginate(4, ['*'], '1pagination');
     
        return view('categories.allNotifications',['postH'=>$postH,
                                                    'cat_id'=>$cat_id,
                                                    'noti'=>$noti,
                                                    'about'=>$about]);
    }
}
