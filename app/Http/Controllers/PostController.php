<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;

use App\Category;
use App\Post;
use App\Notification;
use App\User;
use App\About;
use App\Like;
use App\dislike;
use App\Comment;
use App\Profile;
use App\Adminpost;
use Auth;
use DB;
use Image;
use Storage;

class PostController extends Controller
{
    //

    public function waiting($profile_id){
      $category = Category::all();
      return view('posts.post', ['profile_id' => $profile_id,'category' => $category]);
    }


    public function addPost($profile_id){



      
      if(Auth::guest()){

        return view('Auth.register');
      }


      
        $user_id = Auth::user()->id;
        $testprofile = User::where( 'id', '=', $user_id)->get();
        
            $category = Category::all();
            $noti = Notification::all();
            $cat_id =Category::paginate(10);
            $postH = Post::paginate(4, ['*'], '1pagination');
            $about = About::all();
            
            return view('posts.post',  ['about' => $about,'postH' => $postH,'cat_id' => $cat_id,'noti' => $noti,'testprofile' => $testprofile,'category' => $category]);
      
      
    
   
        
    }



  
public function postAdding(Request $request){

  $this->validate($request,[
    'post_title'=> 'required',
    'post_body'=> 'required',
    'category_id'=> 'required',
  
    'user_id'=> 'user_id',

    
           ]);


           $posts= new post;
           
           $posts -> post_title = $request -> input('post_title');
           
           //$posts -> user_id =auth::user()->id;
           
           $posts -> user_id =auth::user()->id;
           $posts -> user_name =auth::user()->user_name;
           $posts -> profile_pic =auth::user()->profile_pic;
           
           $posts -> post_body = $request ->input('post_body');

           $posts -> category_id= $request ->input('category_id');

           
           
          //  if(Input::hasFile('post_image')){
          //    $file= Input::file('post_image');
          //  $file -> move(public_path(). '/posts/',
          //  $file-> getClientOriginalName());
          //  $url = URL::to("/") . '/posts/'.
          //  $file->getClientOriginalName();  

          //  $posts->post_image = $url;
           
          //  }
           

          if($request->hasFile('post_image')){
            $image = Input::file('post_image');
            $img = Image::make($image)->resize(180,150)->encode();
            $filename = time().'.'.$image->getClientOriginalExtension();          
            Storage::put($filename, $img);
            Storage::move($filename, 'public/adminposts/' . $filename);
            $posts->post_image = $filename;

          }

           
           $posts ->save();
           return redirect('/firstpage')->  
           with('response', 'the post has been Added Successfully');
             }

      public function adminpostform(){
            $user_id = Auth::user()->id;
            $testprofile = User::where( 'id', '=', $user_id)->get();
            $cat_id = Category::paginate(11);
            $noti = Notification::all();
            $about = About::all();
            $postH = Post::paginate(4, ['*'], '1pagination');
            return view('posts.adminpost', ['about' => $about,'postH' => $postH,'noti' => $noti, 'testprofile' => $testprofile,'cat_id' => $cat_id]);
          }

public function adminpostAdding(Request $request){

  $this->validate($request,[
    'post_title'=> 'required',
    'post_body'=> 'required',
    'category_id'=> 'required',
    'post_image'=> 'required',
    //'profile_id'=> 'profile_id',

    
           ]);


           $posts= new Adminpost;
           
           $posts -> post_title = $request -> input('post_title');
           
           $posts -> user_id =auth::user()->id;
           
           $posts -> post_body = $request ->input('post_body');

           $posts -> category_id= $request ->input('category_id');
           
           
        if($request->hasFile('post_image')){
          $image = Input::file('post_image');
          $img = Image::make($image)->resize(100,100)->encode();
          $filename = time().'.'.$image->getClientOriginalExtension();          
          Storage::put($filename, $img);
          Storage::move($filename, 'public/adminposts/' . $filename);
          $posts->post_image = $filename;
        }


           
           $posts ->save();
           return redirect('/')->  
           with('response', 'the post has been Added Successfully');
           
             }


        
public function editpost($id){
 
  
  $user_id = Auth::user()->id;
  $category = Category::all();
  $testprofile = User::where( 'id', '=', $user_id)->get();
  $post = Post::findOrFail($id);
  $noti = Notification::all();
  $cat_id = Category::with('posts')->paginate(10);
  $postH = Post::paginate(4, ['*'], '1pagination');
  $about = About::all();

  //$profile = Profile::find($profiles->user_id);
if( Auth::user()->id == $post->user_id) 
  {
  return view('posts.editpost',  ['about' => $about,'cat_id' => $cat_id,'postH' => $postH,'noti' => $noti,'testprofile' => $testprofile,'category' => $category,
  'post' => $post]);
  }
  return redirect()->back();
}

public function postEdditing(Request $request, $post_id){
  

  if($request->hasFile('post_image')){
    $image = Input::file('post_image');
    $img = Image::make($image)->resize(280,150)->encode();
    $filename = time().'.'.$image->getClientOriginalExtension();          
    Storage::put($filename, $img);
    Storage::move($filename, 'public/adminposts/' . $filename);
    //$posts->post_image = $filename;
  }


  $postUpdate = Post::where('id', $post_id)
                        ->Update([ 
                          'post_title' => $request -> input('post_title'),
                          'post_body' => $request -> input('post_body'),
                          'category_id' => $request -> input('category_id'),
                          'Post_image' => $filename
                          
                        ]);


  return redirect('/firstpage')->  
  with('Success', 'The post has been updated successfuly'); 
                         }



   public function deletePost(Request $request, $post_id){

              post::where('id', $post_id)
              ->delete();

              return redirect()->back();
             
             }
  public function deleteAdminPost(Request $request, $post_id){

        AdminPost::where('id', $post_id)
        ->delete();

        return redirect()->back();
        }

public function category($category_id){     
          $noti = Notification::all();
          $AdminPost = Adminpost::all();  
          $cat_id = Category::with('posts')->paginate(10);
          $about = About::all();
          $cat_id = Category::where( 'id', '=', $category_id)->paginate(10);
          $postH = Post::paginate(4, ['*'], '1pagination');

        $posta = DB::table('posts') 
         ->join('categories', 'posts.category_id', '=', 'categories.id')
          ->select('posts.*', 'categories.category') 
          ->where(['categories.id' => $category_id])
          ->paginate(3);
        $postaa = DB::table('posts') 
         ->join('categories', 'posts.category_id', '=', 'categories.id')
          ->select('posts.*', 'categories.category') 
          ->where(['categories.id' => $category_id])
          ->latest()->paginate(2);
   

        return view('categories.categorisedposts', [
          'about' => $about,
          'noti' => $noti,
          'postaa' => $postaa,
          'postH' => $postH,
          'AdminPost' => $AdminPost,
          'posta' => $posta,
          'cat_id' => $cat_id]);

          
        }
public function postsAll(){     
          $noti = Notification::all();
          $AdminPost = Adminpost::all();  
          $cat_id = Category::with('posts')->paginate(10);
          $about = About::all();
  
          $postH = Post::paginate(4, ['*'], '1pagination');
          $postall = Post::paginate(15);


        return view('posts.allPosts', [
          'about' => $about,
          'noti' => $noti,
          'postH' => $postH,
          'AdminPost' => $AdminPost,    
          'cat_id' => $cat_id,
          'postall' => $postall,
          ]);

          
        }
public function deletePostByadmin($id){     
    Post::find($id)->delete();


        return redirect()->back();

          
        }

    
}
