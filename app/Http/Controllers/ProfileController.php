<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\profile;
use App\Notification;
use App\Category;
use App\Post;
use App\About;
use Auth;


use App\User;










class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response

     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        // $cat = Category::find($category->id);

        // return view('profiles.newProfile', ['cat'=>$cat]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function Indivial($user_id)
    {

        $individual = User::where( 'id', '=', $user_id)->get();

        $tiwe = Post::where( 'user_id', '=', $user_id)->get();
        $realName = User::where( 'id', '=', $user_id)->get();
        $testprofile = User::where( 'id', '=', $user_id)->get();
        $noti = Notification::all();
        $postH = Post::paginate(4, ['*'], '1pagination');
        $cat_id = Category::with('posts')->paginate(10);
        $about = About::all();
        return view('profiles.individualView',['about'=>$about,'cat_id'=>$cat_id,'postH'=>$postH,'noti'=>$noti,'individual'=>$individual,'realName'=>$realName,
        'tiwe'=>$tiwe,'testprofile'=>$testprofile]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
