<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Profile;
use App\Post;
use App\User;
use App\Comment;
use App\Like;
use App\Dislike;
use App\Reply;
use Auth;
use DB;
use URL;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


public function comment(Request $request, $post_id){
    
    if(Auth::guest()){

        $this->validate($request, [ 'comment' => 'required']);

        $comment = new Comment;
      
        $comment->post_id = $post_id;
        $comment ->user_name ='unknown user';
      
        $comment ->comment = $request ->input('comment');
        $comment ->save();
        return redirect()->back();
    // commenting with an ip address
    // $this->validate($request, [ 'comment' => 'required']);
    // $ip = \Request::ip();
    // $user = User::where('ip', $ip)->first();
    // if($user == NULL){
    //     $user = new User;
    //     $user->ip = $ip;
    //     $user->save();

    // }
}
    else{
        $this->validate($request, [ 'comment' => 'required']);

        $comment = new Comment;
        $comment ->user_id = Auth::user()->id;
        $comment ->user_name = Auth::user()->user_name;
        $comment ->profile_pic = Auth::user()->profile_pic;
        $comment->post_id = $post_id;        
        $comment ->comment = $request ->input('comment');
        $comment ->save(); 
        return redirect()->back()->with('responseComment', 'comment posted');
    

    }
}
public function replyAdding(Request $request, $comment_id){
    
    if(Auth::guest()){

        $this->validate($request, [ 'reply' => 'required']);
        $reply = new Reply;
        $reply->comment_id = $comment_id;      
        $reply ->reply = $request ->input('reply');
        $reply ->save();    
        return redirect()->back()->with('responseReply', 'replied');

}
    else{
        $this->validate($request, [ 'reply' => 'required']);
        $reply = new Reply;
        $reply ->user_id = Auth::user()->id;
        $reply->comment_id = $comment_id;
        $reply->user_id = Auth::user()->id;       
        $reply ->reply = $request ->input('reply');
        $reply ->save();  
        return redirect()->back()->with('responseReply', 'replied');
    }
}

}
 

