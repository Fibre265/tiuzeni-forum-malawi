<?php

namespace App\Http\Controllers\Auth;



use App\User;
use Auth;
use Image;
use Storage;
use App\About;
use App\Notification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

     public function showRegistrationForm(){
    
        $about = About::all();
        $noti=Notification::all();

        return view('auth.register', ['about'=>$about,'noti'=>$noti]);

     }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'user_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'district' => [ 'string'],
            'profile_pic'=> ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
     

        if(Input::hasFile('profile_pic')){
            $image = Input::file('profile_pic');
            $img = Image::make($image)->resize(180,150)->encode();
            $filename = time().'.'.$image->getClientOriginalExtension();          
            Storage::put($filename, $img);
            Storage::move($filename, 'public/adminposts/' . $filename);
        return User::create([
            'profile_pic' => $filename,
            'name' => $data['name'],
            'user_name' => $data['user_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'district' => $data['district'],
             
        ]);



  $this ->validate($request,[
            'name' => 'required',
            'district' => 'required',
            'profile_pic' =>'required'
            ]);



    }
}












}