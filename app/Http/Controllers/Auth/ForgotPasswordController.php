<?php

namespace App\Http\Controllers\Auth;
use App\About;
use App\Category;
use App\Post;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */



     
    public function ShowLinkRequestForm(){
        $about=About::all();
        $cat_id = Category::paginate(10);
        $postH = Post::paginate(4, ['*'], '1pagination');
        $noti = Post::all();
        return view('auth.passwords.email',['noti'=>$noti,'postH'=>$postH,'about'=>$about,'cat_id'=>$cat_id]);
    }

    public function __construct()
    {
        $this->middleware('guest');
    }
}
