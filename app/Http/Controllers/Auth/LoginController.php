<?php

namespace App\Http\Controllers\Auth;
use App\Profile;
use App\About;
use App\Notification;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */


        public function showLoginForm(){
            $about=About::all();
            $noti=Notification::all();
            return view('auth.login',['about'=>$about,'noti'=>$noti]);
        }


    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
