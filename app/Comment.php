<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        //  'user_id',  
        'id',
        'user_id',
        'post_id',
        'profile_id',
        'comment'
       
  
    ];
    public function post(){
        return $this->belongsTo(Post::class);

    }
    public function profiles(){
        return $this->belongsTo('App\Profile', 'profile_id');

    }

    public function user(){
        return $this->belongsTo(User::class);

    }
    public function reply(){
        return $this->hasMany(Reply::class);

    }
    public function likesoncomment(){
        return $this->hasMany(Likesoncomment::class);

    }
    public function dislikesoncomment(){
        return $this->hasMany(Dislikesoncomment::class);

    }
}
