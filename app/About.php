<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
        'id',
        'about_tiuzeni_forum',
        'user_task',
        'user_prohibition'

    ];
}
