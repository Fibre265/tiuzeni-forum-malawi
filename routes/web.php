<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/', 'positisController@firstpage');
Route::get('/firstpage', 'positisController@index');

Route::get('/adminpost', 'postController@adminpostform');
Route::post('/adminpostAdding', 'postController@adminpostAdding');
Route::post('/postEdditing/{id}', 'PostController@postEdditing');
Route::get('/deletePost/{id}', 'PostController@deletePost');
Route::get('/deleteAdminPost/{id}', 'PostController@deleteAdminPost');
Route::post('/postAdding', 'PostController@postAdding');
Route::get('/editpost/{id}', 'PostController@editpost');
Route::get('/addPost/{profile_id}', 'PostController@addPost');
Route::get('/waiting/{profile_id}', 'PostController@waiting');
Route::get('/postsAll', 'PostController@postsAll');
Route::get('/deletePostByadmin/{id}', 'PostController@deletePostByadmin');


Route::get('/commentsList/{post_id}', 'positisController@commentsList');
Route::post('/comment/{post_id}', 'HomeController@comment'); 
Route::post('/replyComment/{comment_id}', 'HomeController@replyAdding'); 
Route::get('/deleteComment/{id}', 'positisController@deleteComment');
Route::get('/deleteReply/{id}', 'positisController@deleteReply');
Route::get('//allReplies/{id}', 'positisController@allReplies');
Route::get('/like/{id}', 'positisController@like');
Route::get('/likecomment/{id}', 'positisController@likecomment');
Route::get('/dislikecomment/{id}', 'positisController@dislikecomment');
Route::get('/dislike/{id}', 'positisController@dislike');
Route::get('/like/{id}', 'PostController@like');
Route::post('/search', 'positisController@search');


Route::get('/users', 'positisController@users');
Route::get('/deleteUser/{user_id}', 'positisController@deleteUser');
Route::get('/selfDeleteUser', 'positisController@selfDeleteUser');

Route::get('/addedCategories', 'CategoryController@availableCategories')->name('home');
 Route::post('/addedCategoryStoring', 'CategoryController@store');
 Route::get('/addSlot', 'CategoryController@create');
 Route::get('/deleteCategory/{id}', 'CategoryController@delete');
 Route::get('/category/{id}', 'PostController@category');

 Route::post('/addingNotification', 'CategoryController@addingNotification');
 Route::get('/deleteNotice/{id}', 'CategoryController@deleteNotice');
 Route::get('/seeAllNotification', 'CategoryController@seeAllNotification');

 Route::get('/addAbout', 'CategoryController@addAboutForm');
 Route::post('/aboutCreating', 'CategoryController@aboutCreating');
 Route::get('/aboutView', 'CategoryController@aboutView');
 Route::get('/aboutEdit/{id}', 'CategoryController@aboutEdit');
 Route::post('/aboutEditUpdate/{id}', 'CategoryController@aboutEditUpdate');

Route::get('/create', 'ProfileController@create');
Route::get('/Individuals/{user_id}', 'ProfileController@indivial');
