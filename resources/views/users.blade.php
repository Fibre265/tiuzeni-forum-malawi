@extends('testYield')
@include('layouts.partials.tiuzeniTitle')

@section('availableUsers')



<br>


<div class="container ">

    <div class="row ">
        <div class="col-md-11">
            <div class="card">
      
                

               
			   <h2 class=" pt-4 row justify-content-center" style="font-style: italic ">available users</h2> 

	<table class="table" >
	
			<thead>
			
				<tr>
				
					<th>Full name</th>
					<th>Location</th>
					<th>user name</th>
					<th  > Image</th>
				
				</tr>
			
			</thead>
			
			<tbody>
            @foreach($users as $user)
		
			<tr>
					<td> 
					<a class=" " id="editdelete"  href="#"
                        onclick=
                        "var result = confirm('are you sure you wanna delete the user?  all details including the posts will be erased?');
                            if( result ){
                                    event.preventDefault();
                                    document.getElementById('delete-form').submit();
                          }
                        "
                        
                      ><span class="fa fa fa-trash pb-1" style="background:red; color:white">  </span></a>
					
					
					{{$user->name}}
					
					
					
					
					
					</td>
					<td>{{$user->district}}</td>
					<td>{{$user->user_name}}</td>
				<td> <a href='{{ url("/Individuals/$user->id") }}'><img src="/storage/adminposts/{{ $user->profile_pic}}" class="triza"  alt=""></a></td>
		
			</tr>
		@endforeach
			</tbody>
		</table>
		













                </div>
            </div>
        </div>
    </div>
</div>
@endsection
