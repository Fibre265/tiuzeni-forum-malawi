<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">


    <title>Tiuzeni Forum</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

@include('layouts.partials.style')

 @include('layouts.partials.headerBootstrap')



</head>

<body>
  <!-- container section start -->
  <section id="container" class="" 
  style= "background:#F5F5F5"
>
 

@include('layouts.partials.titleBar')
    <!--header end-->

    <!--sidebar start-->
    @include('layouts.partials.sideBar')
    

    <!--sidebar end-->


    <section id="main-content">
      <section class="wrapper ">
        <!--overview start-->
        <div class="row">
        
          <div class="">
          
          @include('layouts.partials.homePageHeadingOthers')      




                     @if(count($posts)>0)


                    @foreach($posta as $post)   
                    @if($post->user_id >2)               
                      @include("layouts.partials.peoplesposts")
                      @else
    
                      @endif
            @endforeach
           

        @foreach($posta as $post)
        @include("layouts.partials.peoplesposts")
       @endforeach

@else

<h6 class="offset-3 ">
<ol class="breadcrumb fa fa-times  offset-4 " style="color:red; width:230px ">
the search returns 0 posts
<ol>
</h6>
@endif

<h6 class="offset-8" style="font-size:7px">{{$posta->links()}}</h6>





<!-- ma positi aanthu athera pano -->

                </div>
                <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>
              </div>
            </div>

          </div>

        </div>
        <!-- project team & activity end -->

      </section>
      <div class="text-right">
        <div class="credits">


        </div>
      </div>
  <!-- javascripts -->


  <script src="js/sweetalert.js"></script>

 
    <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script>

</body>

</html>
