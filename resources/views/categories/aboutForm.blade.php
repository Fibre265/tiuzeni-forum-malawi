
@extends('testYield')

@section('aboutForm')


<br>

<div class="container">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header"> <i ><h2 class="offset-3">you are posting on abouts tab </h2></i>

                <div class="card-body offset-2">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="panel-body">
                    <div class="row">
                    
                    

                    <form method="POST" action="{{ url('/aboutCreating') }}" 
                            enctype="multipart/form-data">
                        @csrf



                        <div class="form-group row">
                            <label for="about_tiuzeni_forum" class="col-md-4 col-form-label text-md-right">about tiuzeni forum</label>

                            <div class="col-md-6">
                               
                                <textarea id="about_tiuzeni_forum" rows="5"  
                                class="form-control{{ $errors->has('about_tiuzeni_forum') ? ' is-invalid' : '' }}"
                                 name="about_tiuzeni_forum" value="{{ old('about_tiuzeni_forum') }}" required></textarea>
                                </textarea>
                                @if ($errors->has('about_tiuzeni_forum'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about_tiuzeni_forum') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="user_task" class="col-md-4 col-form-label text-md-right">user task</label>

                            <div class="col-md-6">
                               
                                <textarea id="user_task" rows="5"  
                                class="form-control{{ $errors->has('post_body') ? ' is-invalid' : '' }}"
                                 name="user_task" value="{{ old('user_task') }}" required></textarea>
                                </textarea>
                                @if ($errors->has('user_task'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user_task') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="post_body" class="col-md-4 col-form-label text-md-right">User Must not do</label>

                            <div class="col-md-6">
                               
                                <textarea id="user_prohibition" rows="5"  
                                class="form-control{{ $errors->has('user_prohibition') ? ' is-invalid' : '' }}"
                                 name="user_prohibition" value="{{ old('user_prohibition') }}" required></textarea>
                                </textarea>
                                @if ($errors->has('user_prohibition'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user_prohibition') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                            
                  

                   
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-large btn-block">
                                   publish post
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    
                    
                    
                    </div>


                    </div>

                     



                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
