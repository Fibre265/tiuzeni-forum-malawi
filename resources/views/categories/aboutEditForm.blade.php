
@extends('testYield')

@section('abouteditForm')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                <i class="offset-3">you are editing the about tab</i>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="panel-body" style="background-color:">
                    <div class="row">
                    
                    

                    <form  class="" method="POST" style="background-color:" action="{{ url('/aboutEditUpdate', array( $aboutv->id )) }}" 
                            enctype="multipart/form-data">
                        @csrf


<h2>
                        <input type="hidden"
                                method= "put"
                                
                                    /></h2>
                        <div class="form-group row justify-content-center">
                            <label for="user_prohibition" class="col-md-6 col-form-label text-md-right">What user must not do</label>

                            <div class="col-md-6 col-lg-6">
                               
                            <textarea id="user_prohibition" rows="8" class="form-control{{ $errors->has('user_prohibition') ? '
                             is-invalid' : '' }}" name="user_prohibition"  required >{{ $aboutv->user_prohibition }}</textarea>
                              
                                

                                @if ($errors->has('about_tiuzeni_forum'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about_tiuzeni_forum') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <label for="about_tiuzeni_forum" class="col-md-6 col-form-label text-md-right">about tiuzeni forum</label>

                            <div class="col-md-6">
                               
                            <textarea id="about_tiuzeni_forum" rows="8"  class="form-control{{ $errors->has('about_tiuzeni_forum') ? '
                             is-invalid' : '' }}" name="about_tiuzeni_forum"  required >{{ $aboutv->about_tiuzeni_forum }}</textarea>
                              
                                

                                @if ($errors->has('about_tiuzeni_forum'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about_tiuzeni_forum') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div class="form-group row justify-content-center">
                            <label for="user_task" class="col-md-6 col-form-label text-md-right">User task</label>

                            <div class="col-md-6">
                               
                            <textarea id="user_task" rows="8"  class="form-control{{ $errors->has('user_task') ? '
                             is-invalid' : '' }}" name="user_task"  required >{{ $aboutv->user_task }}</textarea>
                              
                                

                                @if ($errors->has('user_task'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user_task') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                      
                                 
                               
                                </s
                     
                        <div class="form-group row mb-0 justify-content-center">
                            <div class="col-md-6 offset-md-6">
                                <button type="submit" class="btn btn-primary btn-large btn-block">
                                   update About
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    
                    
                    
                    </div>


                    </div>

                     



                     
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
