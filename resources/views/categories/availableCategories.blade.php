@extends('testYield')

@section('availableCategories')
@include('layouts.partials.tiuzeniTitle')

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<div class="container" >
    <div class="row " >
        <div class="col-md-11">
            <div class="card" >
                <div class="card-header" style="line-height:1"><i class="offset-4">Available topics 
                @if(session('success'))
                <i class="alert alert-danger pull-left">{{session('success')}}</i>
                @endif
                </i> </div>
@if(count($category)>0)
                @foreach($category as $key=>$categorylisting)
            
        
  
  <a 
       href='{{ url("category/{$categorylisting->id}") }}'><p 
       style="font-style: oblique;font-size:15px; background-color:#336699; color:white" class="pl-4">
       {{++$key}}. {{$categorylisting->category}} 

      @if(auth::id()==7)
       <a style="color:red"  onclick="return confirm('are you sure you want to delete the category')" 
        class="fa fa-trash" href="/deleteCategory/{{$categorylisting->id}}"></a>
       @endif
       </p></a>
@endforeach 

    @else
    <br>
    <br>
    <span  class="" style="background:green; text-align:10px">no categories  available  <a href="{{ url('/addSlot') }}"> add some</a> </span>
                @endif

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


<div class=" modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </div>
</div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
