
@extends('testYield')

@section('aboutView')




<div class="row">
@foreach($about as $abouti)
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="info-box blue-bg">
              <i class="fa fa-times " style="color:red"></i>
             
             
              <div class="title">What you must not do</div>
              <p>{{$abouti->user_prohibition}}</p>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->
          <div class="col-lg-3\4 col-md-4 col-sm-12 col-xs-12">
            <div class="info-box blue-bg">
              <i class="fa fa-thumbs-up" style="color:blue"></i>
             
             
              <div class="title">What you should be doing</div>
              <p style="font-size:11px">{{$abouti->user_task}}</p>
            </div>
            <!--/.info-box-->
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="info-box blue-bg">
              <i class="fa fa-meh-o" style="color:red"></i>
             
             
              <div class="title">About this forum</div>
              <p>{{$abouti->about_tiuzeni_forum}}</p>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->
      


@if(auth::id()==7)
 <a  class="row" href='{{ url("/aboutEdit/$abouti->id") }}'>
                    <button type="submit" class="btn btn-success btn-sm  fa fa-pencil-square-o btn-primary pull-left " 
                    style="background: green"  > <i style="color:white">edit </i>  </button>
                    </a>

@endif
        



@endforeach

       


   




@endsection
