@extends('testYield')

@section('addingFreshCategory')






<div class="container " >
    <div class="row ">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header " ><i class="offset-4">You are adding a fresh topic</i> </div>
       
   
                <form method="POST" style="background-color:#336699" action="{{ url('/addedCategoryStoring') }}">
                        @csrf
                         <br>
                        <div class="form-group row">
                            <label for="category" class="col-md-4 col-form-label
                             text-md-right">{{ __('add a Topic') }}</label>

                            <div class="col-md-6">

                            
                                <input id="category" type="category" class="form-control 
                                @error('category') is-invalid @enderror" name="category" value="Topic" 
                                required autocomplete="category" autofocus>

                               
                                    <span class="invalid-feedback" role="alert">
                                       
                                    </span>
                              
                            </div>
                        </div>

       

           
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('submit') }}
                                </button>

                            </div>
                        </div>
                    </form>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

@endsection