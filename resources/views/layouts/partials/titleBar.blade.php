


















































@include('layouts.partials.style')

<header class="header dark-bg" >


      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>




      <a href="/" class="logo pr-4"  style="font-size:18px ; ">   
                   <img src="{{ URL('/images/final.jpg')}}" 
                    class="triza" alt="">TIUZENI <span class="lite"  >FORUM</span>
                    
                    
                       
                    </a>

      <!--logo start-->
   


         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                   <i style="color:white" class="fa fa-bars"></i>
               </button>
    

  <ul class=" pull-right top-menu">

<!-- task notificatoin start -->












      <div class="collapse navbar-collapse navbar-right navbar-main-collapse ">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">

          <!-- task notificatoin start -->









          <li id="" class="dropdown pt-4" >
            <a data-toggle="dropdown" class="dropdown-toggle" style="background:black" href="#">

                            <i class="fa fa-home"></i>
                            <span class="badge bg-important" style="font-size:10px">home</span>
                        </a>
            <ul class="dropdown-menu extended notification">
              <div class="notify-arrow notify-arrow-blue"></div>
              <li>
              <a class="blue  nav-link fa fa-blind" style="background:black" href="{{url('/')}}">Welcome page</a>
</li>

      <li>
              <a class="  nav-link fa fa-home" style="background:black" href="{{url('/firstpage')}}">Home page</a>
                
              </li>
                </ul>






























          <!-- task notificatoin end -->
          <!-- inbox notificatoin start-->
          <li id="" class="dropdown pt-4"> 
            <a data-toggle="dropdown" class="dropdown-toggle" style="background:black" href="#">
                            <i class="fa fa-plug"></i>
                            <span class="badge bg-important " style="font-size:10px">topics</span>
                        </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-blue"></div>
         
              <li  style="background:black">
                <a href="#"  style="background:black">
                @guest
                
                      
@else

<a class="nav-link pull-left"  style="background:black" href="{{ url('/addSlot') }}">add current affairs topic</a>

     @guest
<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
@else
 
@endguest
        

   
@endguest
                                </a>
              </li>

             
              <li>
                
                <a class="nav-link pull-left"  style="background:black" href="{{ url('/addedCategories') }}">available topics</a>
                                
              </li>
          
            </ul>
          </li>
          <!-- task notificatoin end -->


          <!-- start here-->
          <li id="" class="dropdown pt-4
          "> 
            <a data-toggle="dropdown" class="dropdown-toggle" style="background:black" href="#">
                            <i class="fa fa-suitcase"></i>
                            <span class="badge bg-important " style="font-size:10px">about </span>
                        </a>
            <ul class="dropdown-menu extended inbox" style="background:black" >
              <div class="notify-arrow notify-arrow-blue"></div>
              <li>
                <p class="blue">about Tiuzeni Forum (I must read)</p>
              </li>
              <li>
              <p style="font-size:12px; background:black" >
                
            @foreach($about as $about)
            {{ substr($about->about_tiuzeni_forum, 0, 400) }}..
           
            @endforeach
            
@if(auth::id() < 3)
        @guest

              
               @else
               <a class="" href="{{ url('/addAbout') }}">add once</a> 
               @endguest

               @endif

               </p>
   

                                
              </li>

               
              <li>
               
              <a href="{{ url('/aboutView') }}">read more</a>

                                
              </li>
          
            </ul>
          </li>
<!-- end here -->




          <!-- alert notification start-->
          <li id="" class="dropdown pt-4" >
            <a data-toggle="dropdown" class="dropdown-toggle" style="background:black" href="#">

                            <i class="fa fa-bell-o"></i>
                            <span class="badge bg-important">7</span>
                        </a>
            <ul class="dropdown-menu extended notification" style="background:black">
              <div class="notify-arrow notify-arrow-blue"></div>
              <li>
                <p class="blue">notifications panel</p>

@if(count($noti)>0)
@foreach($noti as $notices)
    <i class="small italic" >
                <a  href='{{  url("/deleteNotice/{$notices->id}") }}'><i style="font-size:10px"><i class="fa fa-bell"></i>{{$notices->notice}}</i>
                @if(auth::id() ==1)
                 <span class="  pull-right fa fa fa-trash"  style="color: red"></span> </a>
           @endif
              </i>
@endforeach
@else
<p>no notifications available yet</p>
@endif


@if(auth::id() ==1)
@guest
@else
              <form method="POST"  action="{{ url('/addingNotification') }}">                       {{csrf_field()}}
                       
        
                        <div id="" class="form-group">
                            <textarea  id="" column="2" class="form-control"
                            name="notice" required autofocus></textarea>
                            <div id="" class="form-group pull-left">
                            <button type="submit" 
                            class="btn btn-success btn-sm btn-primary pull-right"
                             style="background: green"  > add </button>
                        </div>
                        </div>

                    </form>
@endguest
@endif
              </li>
             
              @if(count($noti)>0)
      
                <a href="{{'/seeAllNotification'}}">See all notifications</a>
                @endif
              
      
      
            </ul>
          </li>

          
          <!-- alert notification end-->
          <!-- user login dropdown start-->


          <li class="dropdown pt-4" style="background:">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                            @guest
                            <img style="border-radius: 80%; max-width: 14px" src="{{ URL('images/default.png')}}" 
                     alt="">
                            <span class="badge bg-important 
                            " >not logged</span>
                            
                      
                           
                           @else
                           <img style="border-radius: 80%; max-width: 14px" src="storage/adminposts/{{ Auth::user()->profile_pic }}" 
                    class="trizala" alt="">
                           <span class="badge bg-important 
                           ">logged</span>
                            </span>
                            <span class="username" style="font-size:10px" >{{ Auth::user()->name }}</span>
                            
                    
                            <span class="profile-ava">
                  
                                
                            </span>
                          
                            @endguest
                        </a>
            <ul class="dropdown-menu " style="background:black">
              <div class="log-arrow-up"></div>
              <li class="eborder-top">
          
              </li>
           
           
              <li>   @guest 
                     
                      @else
                            
                                @endguest
              </li>
              <li>
              @guest
              @else
              <a class="nav-link" style="font-size:13px" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> 
                                        <i class="fa fa-user">{{ __('Logout') }} </i>
                                    </a>
     
                                                    

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    @endif
              </li>
        
              <li>
              @guest
              <a class="nav-link fa fa-user" href="{{ route('login') }}">{{ __('Login') }}</a>
              <a class="nav-link fa fa-user"  href="{{ route('register') }}">{{ __('register') }}</a>
              @else

                            <a href="#"
                        onclick=
                        "var result = confirm('this will delete your account and all of your posts. are you sure you want to continue?');
                            if( result ){
                                    event.preventDefault();
                                    document.getElementById('delete-form').submit();
                          }
                        "
                      ><span class="fa fa-pencil-square-o pb-1" style="font-size:12px; color:red">  deregister </span></a>
                      
                      
                      <form id="delete-form" action='{{ url("/selfDeleteUser") }}'
                        method="GET" style="display: none;">
                            <input type="hidden" name="_method" value="delete">
                            {{ csrf_field() }}
                      
                      </form>

                      @endguest
              </li>
          
            </ul>
          </li>
          <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
      </div>
    </header>