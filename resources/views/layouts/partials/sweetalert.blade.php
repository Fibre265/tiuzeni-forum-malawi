  <!-- javascripts for sweetalert -->
  <script src="js/sweetalert.min.js"></script>

            <script type="text/javascript">

             @if( notify()->ready() )
              swal({
                title: "{!! notify()->messege() !!}",
                type: "{{ notify()->type() }}"
                @if( notify()->option('timer') )

                  time: "{{ notify()->option('timer') }}",
                  showConfirmButton: false,

                  @endif
                  html: true
              });
            @endif
            </script>

