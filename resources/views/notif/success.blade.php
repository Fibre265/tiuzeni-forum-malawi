@if (session()->has('success'))
<div class="alert alert-dismisable alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="close">
        <span aria-hidden="true">&times;</span>
        </span>
    </button>

    <strong>
        {!! session()->get('success') !!}
    </strong>
</div>
@endif
