
@extends('testYield')

@section('addPostForm')


<br>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <i ><h2 class="offset-2">You are posting a new post </h2></i>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="panel-body">
                    <div class="row">
                    
                    

                    <form method="POST" action="{{ url('/postAdding') }}" 
                            enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="post_title" class="col-md-4 col-form-label text-md-right"> Title</label>

                            <div class="col-md-6">
                                <input id="post_title" type="text" class="form-control{{ $errors->has('post_title') ? ' is-invalid' : '' }}" name="post_title" 
                                value="{{ old('post_title') }}" required autofocus>

                                @if ($errors->has('post_title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('post_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

<h2>
                        <input type="hidden"
                                method= "put"
                                
                                    /></h2>
                        <div class="form-group row">
                            <label for="post_body" class="col-md-4 col-form-label text-md-right">Post text</label>

                            <div class="col-md-6">
                               
                                <textarea id="post_body" rows="5"  
                                class="form-control{{ $errors->has('post_body') ? ' is-invalid' : '' }}"
                                 name="post_body" value="{{ old('post_body') }}" required></textarea>
                                </textarea>
                                @if ($errors->has('post_body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('post_body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_id" class="col-md-4 col-form-label text-md-right">CATEGORY</label>

                            <div class="col-md-6">
                               
                                <select id="category_id" type="category_id" 
                                class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" 
                                name="category_id" required>
                                
                                <option value="">select</option>
                                @if(count($category) > 0)
                                  @foreach($category->all() as $car)
                                    <option value="{{ $car->id}}">{{$car->category}}</option>

                                  
                                  @endforeach
                                  @endif
                                


                                 
                               
                                </select>
                                @if ($errors->has('category_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    
                            <div></div>
                            
                  

                        <div class="form-group row">
                            <label for="post_image" class="col-md-4 col-form-label
                             text-md-right">feature an image</label>

                            <div class="col-md-6">
                                <input id="post_image" type="file" 
                                class="form-control" 
                                name="post_image" >

                     
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-large btn-block">
                                   publish post
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    
                    
                    
                    </div>


                    </div>

                     



                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
